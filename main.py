iteraciones = 6
contador = 0

frase = "Hola Mundo"
vocales = "aeiou"
frase_nueva = ""

while (contador < iteraciones):
    print("Hola Mundo")
    contador += 1
print("Finalizado While")

for i in frase:
    if i in vocales:
        print(i)
        frase_nueva = frase_nueva + i

print(frase_nueva)
print("Finalizado For")


# This is a sample Python script.

# Press Mayús+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


#def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
#    print("Hi, {0}".format(name))  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
#if __name__ == '__main__':
#    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
